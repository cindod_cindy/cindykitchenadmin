
package com.adminapp.cindykitchenadmin.pojo.all_weekend;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class PojoAdminGetAllWeekend {

    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("week_variable")
    @Expose
    private String weekVariable;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PojoAdminGetAllWeekend() {
    }

    /**
     * 
     * @param createdAt
     * @param weekVariable
     * @param id
     * @param updatedAt
     */
    public PojoAdminGetAllWeekend(String createdAt, String updatedAt, long id, String weekVariable) {
        super();
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.id = id;
        this.weekVariable = weekVariable;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public PojoAdminGetAllWeekend withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PojoAdminGetAllWeekend withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PojoAdminGetAllWeekend withId(long id) {
        this.id = id;
        return this;
    }

    public String getWeekVariable() {
        return weekVariable;
    }

    public void setWeekVariable(String weekVariable) {
        this.weekVariable = weekVariable;
    }

    public PojoAdminGetAllWeekend withWeekVariable(String weekVariable) {
        this.weekVariable = weekVariable;
        return this;
    }

}
