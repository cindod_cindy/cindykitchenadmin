package com.adminapp.cindykitchenadmin.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.admin_input_warn.PojoAdminInputWarn;
import com.adminapp.cindykitchenadmin.pojo.admin_send_weekend.PojoAdminSendWeekend;
import com.adminapp.cindykitchenadmin.pojo.post_verification_by_admin.PojoAdminPostVerification;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitHandleApi;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitMethodInterface;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputWeekendActivity extends AppCompatActivity {
    private EditText editText_input_weekend;
    private TextView textView_send_weekend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_weekend);
        editText_input_weekend=findViewById(R.id.et_input_weekend);
        textView_send_weekend=findViewById(R.id.btn_send_weekend);

        textView_send_weekend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editText_input_weekend.getText().toString().isEmpty()){
                    editText_input_weekend.setError("weekend belum diisi");


                }else {
                    adminSendWeekend();


                }
            }
        });
    }

    public void adminSendWeekend(){

        //String verif_cOde = editText_verif_code.getText().toString();
        String weekend = editText_input_weekend.getText().toString();
        //Long idPayment =Long.valueOf(textView_id.getText().toString());

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("week_variable", weekend);
        //jsonObject.addProperty("verification_code", verif_cOde);

        RetrofitMethodInterface methodsFactory =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoAdminSendWeekend> sendWeekendCall= methodsFactory.postWeekend(jsonObject);
        sendWeekendCall.enqueue(new Callback<PojoAdminSendWeekend>() {
            @Override
            public void onResponse(Call<PojoAdminSendWeekend> call, Response<PojoAdminSendWeekend> response) {
                if(response.isSuccessful()){
//                    PojoAdminPostVerification pojoAdminPostVerification=response.body();
//                    pojoAdminPostVerification.getVerificationCode();
//////                    Intent intent = new Intent(A.this,Login.class);
//                    startActivity(intent);
                 /*   spHandle.saveSPBoolean(SpHandle.SP_HAVE_LOGIN, true);
                    startActivity(new Intent(Regis.this, Login.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                  */

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(InputWeekendActivity.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(InputWeekendActivity.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(InputWeekendActivity.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(InputWeekendActivity.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoAdminSendWeekend> call, Throwable t) {
                Toast.makeText(InputWeekendActivity.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }
}