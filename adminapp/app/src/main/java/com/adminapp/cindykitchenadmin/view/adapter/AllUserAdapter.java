package com.adminapp.cindykitchenadmin.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.all_user.PojoAllUser;

import org.w3c.dom.Text;

import java.util.List;

public class AllUserAdapter extends RecyclerView.Adapter<AllUserAdapter.AllUserChild> {

    private List<PojoAllUser> pojoAllUserList;
    private Context context;

    public AllUserAdapter(List<PojoAllUser> pojoAllUserList, Context context){
        this.pojoAllUserList=pojoAllUserList;
        this.context=context;
    }

    @NonNull
    @Override
    public AllUserChild onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list, parent, false);
        AllUserChild mViewHolder = new AllUserChild(mView);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AllUserChild holder, int position) {
        final PojoAllUser pojoAllUser= pojoAllUserList.get(position);
        holder.textView_id.setText(String.valueOf(pojoAllUser.getId()));
        holder.textView_nama.setText(pojoAllUser.getUsername());
        holder.textView_email.setText(pojoAllUser.getEmail());


    }

    @Override
    public int getItemCount() {
        return pojoAllUserList.size();
    }

    public class AllUserChild extends RecyclerView.ViewHolder{

        public TextView textView_id,textView_nama, textView_phone, textView_email, textView_kode_verif, textView_date_pay;


        public AllUserChild(@NonNull View itemView) {
            super(itemView);
            textView_id=itemView.findViewById(R.id.tv_all_user_id);
            textView_nama=itemView.findViewById(R.id.tv_all_user_name);
            textView_phone=itemView.findViewById(R.id.tv_all_user_phone);
            textView_email=itemView.findViewById(R.id.tv_all_user_email);
            textView_kode_verif=itemView.findViewById(R.id.tv_all_user_kode_verif);
            textView_date_pay=itemView.findViewById(R.id.tv_all_user_date_pay);
        }
    }

}
