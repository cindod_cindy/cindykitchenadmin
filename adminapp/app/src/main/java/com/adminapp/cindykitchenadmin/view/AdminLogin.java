package com.adminapp.cindykitchenadmin.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.regis.PojoRegis;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitHandleApi;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitMethodInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminLogin extends AppCompatActivity {

    private EditText editText_nama, editText_phone, editText_email, editText_password;
    private TextView textView_admin_login_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        editText_nama=findViewById(R.id.et_admin_nama);
        editText_phone=findViewById(R.id.et_admin_telepon);
        editText_email=findViewById(R.id.et_admin_email);
        editText_password=findViewById(R.id.et_admin_password);

        textView_admin_login_btn=findViewById(R.id.tv_admin_login_btn);

        textView_admin_login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(editText_nama.getText().toString().isEmpty()&&editText_email.getText().toString().isEmpty()&& editText_phone.getText().toString().isEmpty()&&editText_password.getText().toString().isEmpty()){
                    editText_nama.setError("nama belum diisi");
                    editText_phone.setError("nomor telepon belum diisi");
                    editText_email.setError("email belum diisi");
                    editText_password.setError("password belum diisi");


                }else {
                    adminRegis();


                }

            }
        });
    }

    public void adminRegis(){

        String name = editText_nama.getText().toString();
        String phone = editText_phone.getText().toString();
        String email = editText_email.getText().toString();
        String password = editText_password.getText().toString();

        JsonObject jsonObject = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        jsonArray.add("admin");
        jsonArray.add("admin");

        jsonObject.addProperty("name", name);
        jsonObject.addProperty("phone", phone);
        jsonObject.addProperty("email",email );
        jsonObject.addProperty("password", password);
        jsonObject.add("role", jsonArray);

        RetrofitMethodInterface methodsFactory =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoRegis> call= methodsFactory.isRegistration(jsonObject);
        call.enqueue(new Callback<PojoRegis>() {
            @Override
            public void onResponse(Call<PojoRegis> call, Response<PojoRegis> response) {
                if(response.isSuccessful()){
                    PojoRegis pojoRegis=response.body();
                    pojoRegis.getMessage();
                    Intent intent = new Intent(AdminLogin.this,Login.class);
                    startActivity(intent);
                 /*   spHandle.saveSPBoolean(SpHandle.SP_HAVE_LOGIN, true);
                    startActivity(new Intent(Regis.this, Login.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                  */

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(AdminLogin.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(AdminLogin.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(AdminLogin.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(AdminLogin.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoRegis> call, Throwable t) {
                Toast.makeText(AdminLogin.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }
}