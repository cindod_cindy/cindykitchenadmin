package com.adminapp.cindykitchenadmin.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.post_verification_by_admin.PojoAdminPostVerification;
import com.adminapp.cindykitchenadmin.pojo.regis.PojoRegis;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitHandleApi;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitMethodInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputVerificationCode extends AppCompatActivity {

    private TextView textView_id, textView_username, textView_btn_send_verif_code;
    private EditText editText_verif_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_verification_code);
        textView_id=findViewById(R.id.tv_verif_payment_id);
        textView_username=findViewById(R.id.tv_verif_username);
        textView_btn_send_verif_code=findViewById(R.id.tv_btn_verif_send_code);
        editText_verif_code=findViewById(R.id.et_verif_verification_code);

        textView_btn_send_verif_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editText_verif_code.getText().toString().isEmpty()){
                    editText_verif_code.setError("nama belum diisi");


                }else {
                    sendVerifByAdmin();


                }
            }
        });
    }

    public void sendVerifByAdmin(){

        String verif_cOde = editText_verif_code.getText().toString();
        String user_name = textView_username.getText().toString();
        Long idPayment =Long.valueOf(textView_id.getText().toString());

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("username", user_name);
        jsonObject.addProperty("verification_code", verif_cOde);

        RetrofitMethodInterface methodsFactory =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoAdminPostVerification> call= methodsFactory.postVerification(idPayment,jsonObject);
        call.enqueue(new Callback<PojoAdminPostVerification>() {
            @Override
            public void onResponse(Call<PojoAdminPostVerification> call, Response<PojoAdminPostVerification> response) {
                if(response.isSuccessful()){
                    PojoAdminPostVerification pojoAdminPostVerification=response.body();
                    pojoAdminPostVerification.getVerificationCode();
////                    Intent intent = new Intent(A.this,Login.class);
//                    startActivity(intent);
                 /*   spHandle.saveSPBoolean(SpHandle.SP_HAVE_LOGIN, true);
                    startActivity(new Intent(Regis.this, Login.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                  */

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(InputVerificationCode.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(InputVerificationCode.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(InputVerificationCode.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(InputVerificationCode.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoAdminPostVerification> call, Throwable t) {
                Toast.makeText(InputVerificationCode.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }
}