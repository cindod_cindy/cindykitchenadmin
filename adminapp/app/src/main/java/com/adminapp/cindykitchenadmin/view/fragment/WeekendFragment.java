package com.adminapp.cindykitchenadmin.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.all_weekend.PojoAdminGetAllWeekend;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitHandleApi;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitMethodInterface;
import com.adminapp.cindykitchenadmin.shared_pref.AdminSharedPref;
import com.adminapp.cindykitchenadmin.view.adapter.WeekendAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WeekendFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WeekendFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public WeekendFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WeekendFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WeekendFragment newInstance(String param1, String param2) {
        WeekendFragment fragment = new WeekendFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private RecyclerView recyclerView;
    private WeekendAdapter weekendAdapter;
    private List<PojoAdminGetAllWeekend> pojoAdminGetAllWeekendList = new ArrayList<>();
    private RetrofitMethodInterface retrofitMethodInterface;
    private AdminSharedPref adminSharedPref;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View view = inflater.inflate(R.layout.fragment_weekend, container, false);

        recyclerView = view.findViewById(R.id.rv_weekend_list_fragment);
        weekendAdapter = new WeekendAdapter( pojoAdminGetAllWeekendList,getContext());
        recyclerView.setAdapter(weekendAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        getAllWeekendList();
        return view;

    }

    public void getAllWeekendList(){
        Long id = adminSharedPref.getSpUserId();

        //String tokenUser = spHandle.getSpTokenUser();
//        Map<String,String> token = new HashMap<>();
//        token.put("Authorization", "Bearer "+tokenUser);


        retrofitMethodInterface = RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoAdminGetAllWeekend> pojoAdminGetAllWeekendCall=retrofitMethodInterface.getAllWeekend();
        pojoAdminGetAllWeekendCall.enqueue(new Callback<PojoAdminGetAllWeekend>() {
            @Override
            public void onResponse(Call<PojoAdminGetAllWeekend> call, Response<PojoAdminGetAllWeekend> response) {

                if (response.isSuccessful()) {
                    // response.body().getData();
//                    List<PojoAllUser> content = response.body();
//                    confirmAdapter = new ConfirmAdapter(content,getContext());
                    recyclerView.setAdapter(weekendAdapter);
                    //adapterIndonesia = new AdapterIndonesia(getContext(),propinsiAtributes);
                    //recyclerView.setAdapter(adapterIndonesia);
                    weekendAdapter.notifyDataSetChanged();
                }
                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(getContext(), "404 not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(getContext(), "500 internal server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(getContext(), "401 unauthorized", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(getContext(), "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<PojoAdminGetAllWeekend> call, Throwable t) {
                Toast.makeText(getContext(), "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });


    }
}