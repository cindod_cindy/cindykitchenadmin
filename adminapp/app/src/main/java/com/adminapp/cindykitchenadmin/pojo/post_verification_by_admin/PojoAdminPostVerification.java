
package com.adminapp.cindykitchenadmin.pojo.post_verification_by_admin;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class PojoAdminPostVerification {

    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("verification_code")
    @Expose
    private String verificationCode;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PojoAdminPostVerification() {
    }

    /**
     * 
     * @param createdAt
     * @param id
     * @param updatedAt
     * @param username
     * @param verificationCode
     */
    public PojoAdminPostVerification(String createdAt, String updatedAt, long id, String username, String verificationCode) {
        super();
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.id = id;
        this.username = username;
        this.verificationCode = verificationCode;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public PojoAdminPostVerification withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PojoAdminPostVerification withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PojoAdminPostVerification withId(long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public PojoAdminPostVerification withUsername(String username) {
        this.username = username;
        return this;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public PojoAdminPostVerification withVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
        return this;
    }

}
