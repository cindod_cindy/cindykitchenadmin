package com.adminapp.cindykitchenadmin.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.login.PojoLogin;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitHandleApi;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitMethodInterface;
import com.adminapp.cindykitchenadmin.shared_pref.AdminSharedPref;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    private EditText editText_nama_login, editText_password_login;
    private TextView textView_admin_login_btn;

    private AdminSharedPref adminSharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editText_nama_login=findViewById(R.id.admin_login_nama);
        editText_password_login=findViewById(R.id.admin_login_pass);
        textView_admin_login_btn=findViewById(R.id.admin_login_btn);

        adminSharedPref = new AdminSharedPref(Login.this);

        if (adminSharedPref.getSPSudahLogin()){
            startActivity(new Intent(Login.this, BottomNavActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }

        textView_admin_login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editText_nama_login.getText().toString().isEmpty() && editText_password_login.getText().toString().isEmpty()){
                    editText_nama_login.setError("name can't be empty");
                    editText_password_login.setError("password can't be empty");


                }else {
                    loginAdmin();

                }

            }
        });
    }

    public void loginAdmin(){

        String username = editText_nama_login.getText().toString();
        String password = editText_password_login.getText().toString();

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("username", username);
        jsonObject.addProperty("password", password);

        RetrofitMethodInterface retrofitMethodInterface =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoLogin> call= retrofitMethodInterface.isLoginValid(jsonObject);
        call.enqueue(new Callback<PojoLogin>() {
            @Override
            public void onResponse(Call<PojoLogin> call, Response<PojoLogin> response) {
                if(response.isSuccessful()){

                    PojoLogin pojoLogin= response.body();

                    adminSharedPref.saveName(AdminSharedPref.SP_NAME,editText_nama_login.getText().toString());
                    adminSharedPref.savePassword(AdminSharedPref.SP_PASSWORD,editText_password_login.getText().toString());
                    // Shared Pref ini berfungsi untuk menjadi trigger session login
                    adminSharedPref.saveSPBoolean(AdminSharedPref.SP_SUDAH_LOGIN, true);
                    startActivity(new Intent(Login.this, BottomNavActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(Login.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(Login.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(Login.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(Login.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoLogin> call, Throwable t) {
                Toast.makeText(Login.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }
}