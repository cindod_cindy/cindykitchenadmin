package com.adminapp.cindykitchenadmin.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.admin_input_warn.PojoAdminInputWarn;
import com.adminapp.cindykitchenadmin.pojo.get_done_list.PojoAdminGetDone;
import com.adminapp.cindykitchenadmin.pojo.post_verification_by_admin.PojoAdminPostVerification;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitHandleApi;
import com.adminapp.cindykitchenadmin.retrofithandle.RetrofitMethodInterface;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputWarnActivity extends AppCompatActivity {

    private TextView textView_btn_send_warn, textView_done_id, textView_username;
    private EditText editText_input_warn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_warn);
        editText_input_warn=findViewById(R.id.et_warn_input_warn);
        textView_btn_send_warn=findViewById(R.id.tv_btn_send_warn);
        textView_done_id=findViewById(R.id.et_warn_done_id);
        textView_username=findViewById(R.id.et_warn_username);

        textView_btn_send_warn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editText_input_warn.getText().toString().isEmpty()){
                    editText_input_warn.setError("warn belum diisi");


                }else {
                    sendWarn();


                }

            }
        });
    }


    public void sendWarn(){

        String warn = editText_input_warn.getText().toString();
        String user_name = textView_username.getText().toString();
        Long idPayment =Long.valueOf(textView_done_id.getText().toString());

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("warn_variable", warn);
        jsonObject.addProperty("username", user_name);


        RetrofitMethodInterface methodsFactory =  RetrofitHandleApi.getRetrofitLink().create(RetrofitMethodInterface.class);
        Call<PojoAdminInputWarn> inputWarnCall= methodsFactory.postWarn(idPayment,jsonObject);
        inputWarnCall.enqueue(new Callback<PojoAdminInputWarn>() {
            @Override
            public void onResponse(Call<PojoAdminInputWarn> call, Response<PojoAdminInputWarn> response) {
                if(response.isSuccessful()){
//                    PojoAdminPostVerification pojoAdminPostVerification=response.body();
//                    pojoAdminPostVerification.getVerificationCode();
////                    Intent intent = new Intent(A.this,Login.class);
//                    startActivity(intent);
                 /*   spHandle.saveSPBoolean(SpHandle.SP_HAVE_LOGIN, true);
                    startActivity(new Intent(Regis.this, Login.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();

                  */

                }

                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(InputWarnActivity.this, " not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(InputWarnActivity.this, "server error", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            Toast.makeText(InputWarnActivity.this, " sorry can't authenticated, try again", Toast.LENGTH_SHORT).show();
                            break;

                        default:
                            Toast.makeText(InputWarnActivity.this, "unknown error ", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<PojoAdminInputWarn> call, Throwable t) {
                Toast.makeText(InputWarnActivity.this, "network failure :( inform the user and possibly retry ", Toast.LENGTH_SHORT).show();

            }
        });

    }
}