package com.adminapp.cindykitchenadmin.retrofithandle;

import com.adminapp.cindykitchenadmin.pojo.admin_input_warn.PojoAdminInputWarn;
import com.adminapp.cindykitchenadmin.pojo.admin_send_weekend.PojoAdminSendWeekend;
import com.adminapp.cindykitchenadmin.pojo.all_user.PojoAllUser;
import com.adminapp.cindykitchenadmin.pojo.all_warn.PojoAdminGetAllWarnPayment;
import com.adminapp.cindykitchenadmin.pojo.all_weekend.PojoAdminGetAllWeekend;
import com.adminapp.cindykitchenadmin.pojo.get_done_list.PojoAdminGetDone;
import com.adminapp.cindykitchenadmin.pojo.login.PojoLogin;
import com.adminapp.cindykitchenadmin.pojo.payment.Content;
import com.adminapp.cindykitchenadmin.pojo.post_verification_by_admin.PojoAdminPostVerification;
import com.adminapp.cindykitchenadmin.pojo.regis.PojoRegis;
import com.google.gson.JsonObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RetrofitMethodInterface {
    @Headers({
            "Content-Type:application/json"
    })
    @POST("/auth/signup")
    Call<PojoRegis> isRegistration(@Body JsonObject body);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("auth/signin")
    Call<PojoLogin> isLoginValid(@Body JsonObject body);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("auth/allUser")
    Call<PojoAllUser> getAllUser();

    @Headers({
            "Content-Type:application/json"
    })
    @POST("payment/getpayment")
    Call<Content> getPayment();

    @Headers({
            "Content-Type:application/json"
    })
    @POST("verification/payment/{paymentId}/postverification")
    Call<PojoAdminPostVerification> postVerification(@Path("id") Long id,@Body JsonObject jsonObject);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("payment/donepayment/getAllDone")
    Call<PojoAdminGetDone> getPaymentDdone();

    @Headers({
            "Content-Type:application/json"
    })
    @POST("warnpayment/done/{doneId}/postwarn")
    Call<PojoAdminInputWarn> postWarn(@Path("doneId") Long id, @Body JsonObject jsonObject);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("week/postweek")
    Call<PojoAdminSendWeekend> postWeekend(@Body JsonObject jsonObject);


    @Headers({
            "Content-Type:application/json"
    })
    @POST("warnpayment/adminGetAllWarnUser")
    Call<PojoAdminGetAllWarnPayment> getAllWarn();

    @Headers({
            "Content-Type:application/json"
    })
    @POST("week/getweek")
    Call<PojoAdminGetAllWeekend> getAllWeekend();












}
