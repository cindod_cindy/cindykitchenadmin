
package com.adminapp.cindykitchenadmin.pojo.admin_input_warn;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class PojoAdminInputWarn {

    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("warn_variable")
    @Expose
    private String warnVariable;
    @SerializedName("username")
    @Expose
    private String username;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PojoAdminInputWarn() {
    }

    /**
     * 
     * @param createdAt
     * @param warnVariable
     * @param id
     * @param updatedAt
     * @param username
     */
    public PojoAdminInputWarn(String createdAt, String updatedAt, long id, String warnVariable, String username) {
        super();
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.id = id;
        this.warnVariable = warnVariable;
        this.username = username;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public PojoAdminInputWarn withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PojoAdminInputWarn withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PojoAdminInputWarn withId(long id) {
        this.id = id;
        return this;
    }

    public String getWarnVariable() {
        return warnVariable;
    }

    public void setWarnVariable(String warnVariable) {
        this.warnVariable = warnVariable;
    }

    public PojoAdminInputWarn withWarnVariable(String warnVariable) {
        this.warnVariable = warnVariable;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public PojoAdminInputWarn withUsername(String username) {
        this.username = username;
        return this;
    }

}
