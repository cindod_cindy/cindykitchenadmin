package com.adminapp.cindykitchenadmin.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.all_user.PojoAllUser;
import com.adminapp.cindykitchenadmin.pojo.payment.Content;

import java.util.List;

public class PayementAdapter extends RecyclerView.Adapter<PayementAdapter.PaymentChild> {

    private List<Content> contentListgetPayment;
    private Context context;

    public PayementAdapter(List<Content> contentListgetPayment, Context context){
        this.contentListgetPayment=contentListgetPayment;
        this.context=context;
    }
    @NonNull
    @Override
    public PaymentChild onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_payment_list, parent, false);
        PaymentChild mViewHolder = new PaymentChild(mView);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentChild holder, int position) {
        final Content content= contentListgetPayment.get(position);
        holder.textView_payment_id.setText(String.valueOf(content.getId()));
        holder.textView_username.setText(content.getUsername());
        holder.textView_bank_pengirim.setText(content.getBankpengirim());
        holder.textView_nama_pengirim.setText(content.getNamapengirim());
        holder.textView_jumlah_uang.setText(content.getJumlahUang());
        holder.textView_date_pbayar.setText(content.getCreatedAt());


    }

    @Override
    public int getItemCount() {
        return contentListgetPayment.size();
    }

    public class PaymentChild extends RecyclerView.ViewHolder{

        private TextView  textView_payment_id, textView_username,textView_bank_pengirim, textView_nama_pengirim, textView_jumlah_uang, textView_date_pbayar,
        textView_btn_verif_code;

        public PaymentChild(@NonNull View itemView) {
            super(itemView);
            textView_payment_id=itemView.findViewById(R.id.payment_id);
            textView_username=itemView.findViewById(R.id.payment_username);
            textView_bank_pengirim=itemView.findViewById(R.id.payment_bank_pengirim);
            textView_nama_pengirim=itemView.findViewById(R.id.payment_nama_pengirim);
            textView_jumlah_uang=itemView.findViewById(R.id.payment_jumlah_uang);
            textView_date_pbayar=itemView.findViewById(R.id.payement_date_payement);
            textView_btn_verif_code=itemView.findViewById(R.id.payment_btn_give_verif_code);
        }
    }
}
