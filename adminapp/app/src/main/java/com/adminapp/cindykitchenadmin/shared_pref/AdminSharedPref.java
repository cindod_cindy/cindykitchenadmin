package com.adminapp.cindykitchenadmin.shared_pref;

import android.content.Context;
import android.content.SharedPreferences;

public class AdminSharedPref {

    public static final String CK_ADMIN_APP = "ckAdminApp";

    public static final String SP_NAME = "spName";
    public static final String SP_PASSWORD = "spPassword";
    public static final Long SP_USER_ID=1l;

    public static final String SP_SUDAH_LOGIN = "spSudahLogin";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;

    public AdminSharedPref(Context context){
        sp = context.getSharedPreferences(CK_ADMIN_APP, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void saveSPString(String keySP, String value){
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPInt(String keySP, int value){
        spEditor.putInt(keySP, value);
        spEditor.commit();
    }

    public void setSpUserId(String keyId, Long valueId){
        spEditor.putLong(keyId,valueId);
        spEditor.commit();

    }

    public void saveName(String keyName, String valueName){
        spEditor.putString(keyName,valueName);
        spEditor.commit();
    }

    public void savePassword(String keyPassword, String valuePass){
        spEditor.putString(keyPassword,valuePass);
        spEditor.commit();

    }

    public void saveSPBoolean(String keySP, boolean value){
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public String getSPName(){
        return sp.getString(SP_NAME, "");
    }

    public String getPassword(){
        return sp.getString(SP_PASSWORD, "");
    }

    public Long getSpUserId(){
        return sp.getLong(String.valueOf(SP_USER_ID), Long.valueOf(1L));


    }

    public Boolean getSPSudahLogin(){
        return sp.getBoolean(SP_SUDAH_LOGIN, false);
    }


}
