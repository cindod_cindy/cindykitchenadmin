package com.adminapp.cindykitchenadmin.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.all_weekend.PojoAdminGetAllWeekend;
import com.adminapp.cindykitchenadmin.pojo.get_done_list.PojoAdminGetDone;

import org.w3c.dom.Text;

import java.util.List;

public class WeekendAdapter extends RecyclerView.Adapter<WeekendAdapter.WeekendChild> {

    public Context context;
    public List<PojoAdminGetAllWeekend> getAllWeekendList;

    public WeekendAdapter(List<PojoAdminGetAllWeekend> getAllWeekendList,Context context){
        this.getAllWeekendList=getAllWeekendList;
        this.context=context;
    }

    @NonNull
    @Override
    public WeekendChild onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.weekend_list, parent, false);
        WeekendChild mViewHolder = new WeekendChild(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WeekendChild holder, int position) {
        final PojoAdminGetAllWeekend getAllWeekend= getAllWeekendList.get(position);
        holder.textView_date_warn.setText(getAllWeekend.getCreatedAt());
        holder.textView_variable_warn.setText(getAllWeekend.getWeekVariable());

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class WeekendChild extends RecyclerView.ViewHolder{

        private TextView textView_date_warn, textView_variable_warn;

        public WeekendChild(@NonNull View itemView) {
            super(itemView);
            textView_date_warn=itemView.findViewById(R.id.weekend_date);
            textView_variable_warn=itemView.findViewById(R.id.weekend_variable);
        }
    }
}
