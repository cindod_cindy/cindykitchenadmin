package com.adminapp.cindykitchenadmin.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.all_warn.PojoAdminGetAllWarnPayment;
import com.adminapp.cindykitchenadmin.pojo.get_done_list.PojoAdminGetDone;

import java.util.List;

public class UndonePaymentAdapter extends RecyclerView.Adapter<UndonePaymentAdapter.UndonePaymentChild> {

    public Context context;
    public List<PojoAdminGetAllWarnPayment> getAllWarnPaymentList;

    public UndonePaymentAdapter(List<PojoAdminGetAllWarnPayment> getAllWarnPaymentList,Context context){
        this.getAllWarnPaymentList=getAllWarnPaymentList;
        this.context=context;
    }

    @NonNull
    @Override
    public UndonePaymentChild onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.warn_list, parent, false);
        UndonePaymentChild mViewHolder = new UndonePaymentChild(mView);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UndonePaymentChild holder, int position) {
        final PojoAdminGetAllWarnPayment getAllWarnPayment= getAllWarnPaymentList.get(position);
        holder.textView_date_warn.setText(getAllWarnPayment.getCreatedAt());
        holder.textView_username_warn.setText(getAllWarnPayment.getUsername());
        holder.textView_message_warn.setText(getAllWarnPayment.getWarnVariable());

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class UndonePaymentChild extends RecyclerView.ViewHolder{
        private TextView textView_date_warn, textView_username_warn, textView_message_warn;

        public UndonePaymentChild(@NonNull View itemView) {
            super(itemView);

            textView_date_warn=itemView.findViewById(R.id.warn_date);
            textView_username_warn=itemView.findViewById(R.id.warn_name);
            textView_message_warn=itemView.findViewById(R.id.warn_message);
        }
    }
}
