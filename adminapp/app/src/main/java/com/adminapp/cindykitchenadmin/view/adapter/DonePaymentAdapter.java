package com.adminapp.cindykitchenadmin.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.pojo.get_done_list.PojoAdminGetDone;
import com.adminapp.cindykitchenadmin.pojo.payment.Content;

import java.util.List;

public class DonePaymentAdapter extends RecyclerView.Adapter<DonePaymentAdapter.DonePaymentChild>{

    public Context context;
    public List<PojoAdminGetDone> getDoneList;

    public DonePaymentAdapter( List<PojoAdminGetDone> getDoneList,Context context){
       this.getDoneList=getDoneList;
        this.context=context;

    }

    @NonNull
    @Override
    public DonePaymentChild onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.done_payment_list, parent, false);
        DonePaymentChild mViewHolder = new DonePaymentChild(mView);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DonePaymentChild holder, int position) {
        final PojoAdminGetDone pojoAdminGetDone= getDoneList.get(position);
        holder.textView_id.setText(String.valueOf(pojoAdminGetDone.getId()));
        holder.textView_username.setText(pojoAdminGetDone.getUsername());
        holder.textView_pay_date.setText(pojoAdminGetDone.getCreatedAt());
        holder.textView_kode_verif.setText(pojoAdminGetDone.getVerificationNumber());

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class DonePaymentChild extends RecyclerView.ViewHolder{

        public TextView textView_id, textView_username, textView_pay_date,textView_kode_verif,
        textView_btn;

        public DonePaymentChild(@NonNull View itemView) {
            super(itemView);

            textView_id=itemView.findViewById(R.id.tv_done_pay_id);
            textView_username=itemView.findViewById(R.id.done_pay_username);
            textView_pay_date=itemView.findViewById(R.id.done_pay_date);
            textView_kode_verif=itemView.findViewById(R.id.done_pay_kode_verif);
            textView_btn=itemView.findViewById(R.id.done_pay_btn);
        }
    }
}
