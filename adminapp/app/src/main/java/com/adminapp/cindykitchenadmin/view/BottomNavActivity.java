package com.adminapp.cindykitchenadmin.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.adminapp.cindykitchenadmin.R;
import com.adminapp.cindykitchenadmin.view.fragment.AllUserList;
import com.adminapp.cindykitchenadmin.view.fragment.DonePaymentFragment;
import com.adminapp.cindykitchenadmin.view.fragment.UndonePaymentFragment;
import com.adminapp.cindykitchenadmin.view.fragment.UserPaymentFragment;
import com.adminapp.cindykitchenadmin.view.fragment.WeekendFragment;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BottomNavActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_nav);

        //Menampilkan halaman Fragment yang pertama kali muncul
        getFragmentPage(new AllUserList());

        /*Inisialisasi BottomNavigationView beserta listenernya untuk
         *menangkap setiap kejadian saat salah satu menu item diklik
         */
        BottomNavigationView bottomNavigation = findViewById(R.id.bottomNavigationView);
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                Fragment fragment = null;

                //Menantukan halaman Fragment yang akan tampil
                switch (item.getItemId()){
                    case R.id.all_user:
                        fragment = new AllUserList();
                        break;

                    case R.id.done_payment:
                        fragment = new DonePaymentFragment();
                        break;

                    case R.id.undone_payment:
                        fragment = new UndonePaymentFragment();
                        break;

                    case R.id.payment:
                        fragment = new UserPaymentFragment();
                        break;


                    case R.id.weekend:
                        fragment = new WeekendFragment();
                        break;
                }
                return getFragmentPage(fragment);
            }
        });
    }

    //Menampilkan halaman Fragment
    private boolean getFragmentPage(Fragment fragment){
        if (fragment != null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.page_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}